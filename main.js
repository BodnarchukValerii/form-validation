function validationForm() {

	// mask

	$("#phone").mask("+380 99/999/9999")

	// moment.js + validate.js

	validate.extend(validate.validators.datetime, {
		parse: function (value, options) {
			return +moment.utc(value);
		},
		format: function (value, options) {
			var format = options.dateOnly ? "YYYY-MM-DD" : "YYYY-MM-DD hh:mm:ss";
			return moment.utc(value).format(format);
		}
	});

	// Rules from Validation

	let constraints = {
		username: {
			presence: true,
			format: {
				pattern: /^[a-zA-Zа-яА-Я-]+$/,
				message: "^allows only latin and cyryllic letters and dash ( - )"
			},
			length: {
				minimum: 3,
				message: "must be at least 3 characters"
			}
		},
		birthdate: {
			presence: true,
			datetime: {
				dateOnly: true,
				latest: moment.utc().subtract(18, 'years'),
				message: "^You need to be atleast 18 years old"
			}
		},
		email: {
			presence: true,
			email: {
				message: "^Wrong email"
			}
		},
		password: {
			presence: true,
			length: {
				minimum: 8,
				message: "must be at least 8 characters"
			}
		},
		confirmPassword: {
			presence: true,
			length: {
				minimum: 8,
				message: "must be at least 8 characters"
			}
		}
	};

	// boolean Object

	let validationBooleanInput = {
		username: false,
		birthdate: false,
		email: false,
		password: false,
		confirmPassword: false,
	};

	// Valid/Invalid Classes plus appended text

	function validationAppearance(inputs, getValidate, inputName) {
		if (getValidate) {
			inputs.addClass("invalid");
			inputs.removeClass("valid");
			let repeat = false;
			$(".appended-text").remove();
			if (!repeat) {
				inputs.parent(".input-field").append("<div class='appended-text'>" + getValidate + "</div>");
				repeat = true;
			}
			validationBooleanInput[inputName] = false;
		} else {
			inputs.addClass("valid");
			inputs.removeClass("invalid");
			$(".appended-text").remove();
			validationBooleanInput[inputName] = true;
		}

		let disableSubmit = false;
		for (let key in validationBooleanInput) {
			if (!validationBooleanInput[key]) {
				disableSubmit = true;
				break;
			}
		}
		if (disableSubmit) {
			$("#submitButton").attr('disabled', '')
		} else {
			$("#submitButton").removeAttr('disabled')
		}
	}

	function validatePasswordConfirm(passwordInput, confirmPasswordInput) {
		let passwordName = passwordInput.attr('name'),
			confirmName = confirmPasswordInput.attr('name');
		let validation = validate({
			[passwordName]: passwordInput.val(),
			[confirmName]: confirmPasswordInput.val()
		}, {
			[confirmName]: Object.assign({
					equality: passwordName
				},
				constraints[confirmName], )
		})
		validationAppearance(
			confirmPasswordInput,
			validation ? validation.confirmPassword : null,
			confirmName
		)
	}

	function validateInput(input) {
		let inputName = input.attr('name'),
			inputConstraints = constraints[inputName],
			validation = validate.single(input.val(), inputConstraints);
		validationAppearance(input, validation, inputName);
	}

	$('#password').blur(function () {
		validatePasswordConfirm(
			$(this),
			$('#confirmPassword')
		);
	});

	$('#confirmPassword').blur(function () {
		validateInput($(this));
		validatePasswordConfirm(
			$('#password'),
			$(this)
		);
	});

	$(
		['#username', '#birthdate', '#email', '#password'].join(',')
	).each(function () {
		$(this).on('blur', function () {
			validateInput($(this));
		})
	})
}

validationForm();